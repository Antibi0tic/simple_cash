%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Jul 2016 9:43 PM
%%%-------------------------------------------------------------------
-module(sc_element_sup).
-author("antibi0tic").

-behaviour(supervisor).

%% API
-export([start_link/0, start_child/2]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================
start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, []).

start_child(Value, LeaseTime) ->
	supervisor:start_child(?SERVER, [Value, LeaseTime]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================
init([]) ->
	RestartStrategy = {simple_one_for_one, 0, 1},
	Child = {sc_element, {sc_element, start_link, []},
		temporary, brutal_kill, worker, [sc_element]},
	{ok, {RestartStrategy, [Child]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
