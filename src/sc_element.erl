%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Jul 2016 9:58 PM
%%%-------------------------------------------------------------------
-module(sc_element).
-author("antibi0tic").

-behaviour(gen_server).

%% API
-export([
	start_link/2,
	create/2,
	create/1,
	fetch/1,
	replace/2,
	delete/1
]).

%% gen_server callbacks
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).

-define(SERVER, ?MODULE).
-define(DEFAULT_LEASE_TIME, 60). %% 1 minute

-record(state, {value, start_time, lease_time}).

%%%===================================================================
%%% API
%%%===================================================================
start_link(Value, LeaseTime) ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [Value, LeaseTime], []).

create(Value) ->
	create(Value, ?DEFAULT_LEASE_TIME).

create(Value, LeaseTime) ->
	sc_element_sup:start_child(Value, LeaseTime).

fetch(Pid) ->
	gen_server:call(Pid, fetch).

replace(Pid, Value) ->
	gen_server:cast(Pid, {replace, Value}).

delete(Pid) ->
	gen_server:cast(Pid, delete).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
init([Value, LeaseTime]) ->
	StartTime = current_time(),
	TimeLeft = time_left(StartTime, LeaseTime),
	{ok, #state{value = Value, start_time = StartTime, lease_time = LeaseTime}, TimeLeft}.

handle_call(fetch, _From, #state{value = Value, start_time = StartTime, lease_time = LeaseTime} = State) ->
	TimeLeft = time_left(StartTime, LeaseTime),
	{reply, {ok, Value}, State, TimeLeft}.

handle_cast({replace, Value}, #state{start_time = StartTime, lease_time = LeaseTime} =State) ->
	{noreply, State#state{value = Value}, time_left(StartTime, LeaseTime)};

handle_cast(delete, State) ->
	{stop, normal, State}.

handle_info(timeout, State) ->
	{stop, normal, State}.

terminate(_Reason, _State) ->
	sc_store:delete(self()),
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
current_time() ->
	Now = calendar:local_time(),
	calendar:datetime_to_gregorian_seconds(Now).

time_left(_StartTime, infinity) ->
	infinity;
time_left(StartTime, LeaseTime) ->
	TimeElapsed = current_time() - StartTime,
	case LeaseTime - TimeElapsed of
		Time when Time =< 0 -> 0;
		Time -> Time * 1000
	end.
