%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. Aug 2016 6:30 PM
%%%-------------------------------------------------------------------
-module(sc_event).
-author("antibi0tic").

%% API
-export([start_link/0,
	add_handler/2,
	delete_handler/2,
	lookup/1,
	create/2,
	delete/1,
	replace/2,
	expired/1]).

-define(SERVER, ?MODULE).

start_link() ->
	gen_event:start_link({local, ?SERVER}).

add_handler(Handler, Args) ->
	gen_event:add_handler(?SERVER, Handler, Args).

delete_handler(Handler, Args) ->
	gen_event:delete_handler(?SERVER, Handler, Args).

lookup(Key) ->
	gen_event:notify(?SERVER, {lookup, Key}).

create(Key, Value) ->
	gen_event:notify(?SERVER, {create, {Key, Value}}).

replace(Key, Value) ->
	gen_event:notify(?SERVER, {replace, {Key, Value}}).

delete(Key) ->
	gen_event:notify(?SERVER, {delete, Key}).

expired(Key) ->
	gen_event:notify(?SERVER, {expired, Key}).