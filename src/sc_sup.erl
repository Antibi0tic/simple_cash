%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. Aug 2016 1:25 AM
%%%-------------------------------------------------------------------
-module(sc_sup).
-author("antibi0tic").

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================
start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================
init([]) ->
	RestartStrategy = one_for_one,
	MaxRestarts = 4,
	MaxSecondsBetweenRestarts = 3600,

	SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

	ElementSup = {sc_element_sup, {sc_element_sup, start_link, []},
		permanent, 2000, supervisor, [sc_element]},

	EventManager = {sc_event, {sc_event, start_link, []},
		permanent, 2000, worker, [sc_event]},

	{ok, {SupFlags, [ElementSup, EventManager]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
