%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. Aug 2016 1:54 AM
%%%-------------------------------------------------------------------
-module(sc_event_logger).
-author("antibi0tic").

-behaviour(gen_event).

%% API
-export([add_handler/0, delete_handler/0]).

%% gen_event callbacks
-export([
	init/1,
	handle_event/2,
	handle_call/2,
	handle_info/2,
	terminate/2,
	code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

%%%===================================================================
%%% gen_event callbacks
%%%===================================================================
add_handler() ->
	sc_event:add_handler(?MODULE, []).

delete_handler() ->
	sc_event:delete_handler(?MODULE, []).

%%%===================================================================
%%% gen_event callbacks
%%%===================================================================
init([]) ->
	{ok, #state{}}.

handle_event({create, {Key, Value}}, State) ->
	error_logger:info_msg("create(~w, ~w)~n", [Key, Value]),
	{ok, State};

handle_event({lookup, Key}, State) ->
	error_logger:info_msg("lookup(~w)~n", [Key]),
	{ok, State};

handle_event({replace, {Key, Value}}, State) ->
	error_logger:info_msg("replace(~w, ~w)~n", [Key, Value]),
	{ok, State};

handle_event({delete, Key}, State) ->
	error_logger:info_msg("delete(~w)~n", [Key]),
	{ok, State};

handle_event({expired, Key}, State) ->
	error_logger:info_msg("expired(~w)~n", [Key]),
	{ok, State};

handle_event(Event, State) ->
	error_logger:error_msg("UNKNOWN EVENT:~n~p~n", [Event]),
	{ok, State}.

handle_call(_Request, State) ->
	Reply = ok,
	{ok, Reply, State}.

handle_info(_Info, State) ->
	{ok, State}.

terminate(_Arg, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
