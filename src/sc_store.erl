%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Jul 2016 10:37 PM
%%%-------------------------------------------------------------------
-module(sc_store).
-author("antibi0tic").

-define(TABLE_NAME, ?MODULE).

%% API
-export([
	init/0,
	insert/2,
	lookup/1,
	delete/1
]).

init() ->
	ets:new(?TABLE_NAME, [public, named_table]),
	ok.

insert(Key, Pid) ->
	ets:insert(?TABLE_NAME, {Key, Pid}).

lookup(Key) ->
	case ets:lookup(?TABLE_NAME, Key) of
		[{Key, Pid}] -> {ok, Pid};
		[] -> {error, not_found}
	end.

delete(Pid) ->
	ets:match_delete(?TABLE_NAME, {'_', Pid}).