%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Jul 2016 9:41 PM
%%%-------------------------------------------------------------------
-module(sc_app).
-author("antibi0tic").

-behaviour(application).

%% Application callbacks
-export([start/2,	stop/1]).

%%%===================================================================
%%% Application callbacks
%%%===================================================================
start(_StartType, _StartArgs) ->
	sc_store:init(),
	case sc_sup:start_link() of
		{ok, Pid} ->
			sc_event_logger:add_handler(),
			{ok, Pid};
		Error ->
			{error, Error}
	end.

stop(_State) ->
	ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
